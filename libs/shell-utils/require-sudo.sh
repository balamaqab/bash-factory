##########################################################
#
# REQUIRE SUDO - v2023.01.0
#
###########################################################

# import ../appeareance/messages.sh

function require_sudo() {
    if [[ $UID != 0 ]]; then
        msg_error "FATAL ERROR" "Please run this script with sudo:\n" "  sudo $0 $*"
        exit 1
    fi
}

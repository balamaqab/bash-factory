##########################################################
#
# Prompter - v2023.01.0
#
# import ../libs/appeareance/colors.sh
#
###########################################################

# yes_or_no(question options yes_char)
#  * question: string.
#  * options (optional): string, like "(y/n)" or "(yes/no)".
#  * yes_char (optional): character. "y" as default.
#
# Example:
# yes_or_no "$(color normal,white Do you want to install $(color bold,white The Application)?)" "y/n"
# RESPONSE=$? # to get the response of the prompt: 1 for yes and 0 for no
#
function yes_or_no() {

    QUESTION=$1
    OPTIONS=${2:-"(yes/no)"}
    YES=${3:-"y"}
    echo -ne "$QUESTION $(color gray $OPTIONS) "
    read -n 1 -r RESPONSE
    echo ""
    if [ $RESPONSE == $YES ]; then
        return 1
    fi
    return 0
}

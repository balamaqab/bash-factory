###########################################################
#
# SSH Tunnels - v2022.07.0
# Helpers to manage SSH tunnels
#
# FUNCTIONS
#  * open_ssh_tunnel()
#  * search_ssh_tunnel()
#  * close_ssh_tunnel()
#
###########################################################

# IMPORTS
# import ../appeareance/colors.sh

# VARIABLES
# use a prefix like __ssh-tunnels_my_variable

__ssh_tunnels_default_port=3310

# FUNCTIONS

function open_ssh_tunnel() {
    # Arguments:
    #  * server: username@domain.com
    #  * remote_port: Server port
    #  * local_port (optiona): Port to use the tunnel
    #
    # Example:
    #   open_ssh_tunnel(user@domain.com 3306) # To open a connection for mysql

    SERVER=$1
    REMOTE_PORT=${2:-80}
    LOCAL_PORT=${3:-$__ssh_tunnels_default_port}

    ssh -f -L$LOCAL_PORT:localhost:$REMOTE_PORT $SERVER -N

    PROCESS_ID=$(search_ssh_tunnel $LOCAL_PORT)
    echo $PROCESS_ID
}

function search_ssh_tunnel() {
    # Arguments:
    #  * local_port: Port used to open the tunnel
    #
    # Example:
    #   search_ssh_tunnel(4200)

    LOCAL_PORT=${1:-$__ssh_tunnels_default_port}
    PROCESS_ID=$(ps -ef | grep $LOCAL_PORT | awk '{ print $2 }' | head -n 1)
    echo $PROCESS_ID
}

function close_ssh_tunnel() {
    # Arguments:
    #  * local_port: Port used to open the tunnel
    #
    # Example:
    #   close_ssh_tunnel(34324)

    LOCAL_PORT=${1:-$__ssh_tunnels_default_port}
    PROCESS_ID=$(search_ssh_tunnel $LOCAL_PORT)
    kill -9 $PROCESS_ID
}

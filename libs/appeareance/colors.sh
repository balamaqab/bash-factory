##########################################################
#
# Colors - v2022.07.0
#
# FUNCTIONS
#   * color()
#
###########################################################

__colors_normal=$(tput sgr0)
__colors_bold=$(tput bold)
__colors_underline=$(tput smul)
__colors_nounderline=$(tput rmul)

__colors_color_reset="${__colors_normal}\e[0m"
__colors_color_gray="30"
__colors_color_red="31"
__colors_color_green="32"
__colors_color_yellow="33"
__colors_color_blue="34"
__colors_color_pink="35"
__colors_color_cyan="36"
__colors_color_white="37"
__colors_color_38="38"
__colors_color_39="39"
__colors_color_40="40"

# color(format message)
#  * format: comma separated values with format information.
#     * Text styles available: normal, dim, bold, italic and underline.
#     * Text color available: red, yellow, green, blue, white, pink, cyan and gray
#  * message: any text or variable
#
# Example:
#   echo -e "$(color bold,red FATAL ERROR!) Unexpected error at line $(color yellow 24)."
function color() {
    format=$1
    content=${@:2}
    IFS=',' read -ra styles <<<"$format"

    new_message=""
    color_intensity=0
    for style in "${styles[@]}"; do
        case $style in
        normal)
            color_intensity=0
            ;;
        bold)
            color_intensity=1
            ;;
        dim)
            color_intensity=2
            ;;
        italic)
            color_intensity=3
            ;;
        underline)
            color_intensity=4
            ;;
        underline)
            new_message="$new_message${__colors_underline}"
            ;;
        *)
            var_style="__colors_color_$style"
            new_message="$new_message\033[${color_intensity};${!var_style}m"
            ;;
        esac
    done
    new_message="$new_message$content${__colors_color_reset}"

    echo "$new_message"
}

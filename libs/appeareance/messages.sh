###########################################################
#
# Messages - v2022.07.0
# (description here)
#
# FUNCTIONS
#  * error_and_example()
#
###########################################################

# IMPORTS
# import ../appeareance/colors.sh

# FUNCTIONS

# msg_horizontal_border(length character)
#  * length: default 42
#  * character: default "-"
#
# Example:
#   msg_horizontal_border(50 "=")
function msg_horizontal_border() {
    DIVIDER_LENGTH=${1:-42}
    DIVIDER_CHAR=${2:-"-"}
    DIVIDER=""

    for ((i = 0; i < $DIVIDER_LENGTH; ++i)); do
        DIVIDER=$DIVIDER$DIVIDER_CHAR
    done

    echo $DIVIDER
}

# msg_border(color character content)
#  * color
#  * character
#
# Example:
#   msg_border(dim,yellow "=" Any text that you want to wrap)
function msg_border() {
    CONTENT=${@:3}
    COLOR=${1:-"white"}
    CHARACTER=${2:-"-"}
    DIVIDER_TEXT=$(msg_horizontal_border ${#CONTENT}+2 $CHARACTER)
    DIVIDER=$(color $COLOR $DIVIDER_TEXT)
    echo -e $DIVIDER'\n '$CONTENT'\n'$DIVIDER'\n'
}

# msg_builder(title_color message_color title message)
#  * title_color
#  * message_color
#  * title
#  * message
#
# Example:
#   msg_builder(bold,yellow yellow Warning! A warning)
function msg_builder() {
    TITLE_COLOR=${1:-"bold,white"}
    MESSAGE_COLOR=${2:-"normal,white"}
    TITLE=$3
    MESSAGE=${@:4}
    DIVIDER=$(color $TITLE_COLOR $(msg_horizontal_border ${#MESSAGE}+2))

    echo -e $DIVIDER'\n '$(color $TITLE_COLOR $TITLE)'\n  '$(color $MESSAGE_COLOR $MESSAGE)'\n'$DIVIDER'\n'
}

# msg_informative(title message)
#  * title: a title for the warning message
#  * message: description of the error
#
# Example:
#   msg_informative("Tip" Did you know that...?)
function msg_informative() {
    TITLE=${1:-"Info"}
    MESSAGE=${@:2}
    msg_builder "bold,blue" "white" "$TITLE" "$MESSAGE"
}

# msg_warning(title warning_message)
#  * title: a title for the warning message
#  * warning_message: description of the error
#
# Example:
#   msg_warning("WARNING!" A not so long warning message)
function msg_warning() {
    TITLE=${1:-"Warning!"}
    MESSAGE=${@:2}
    msg_builder "bold,yellow" "yellow" "$TITLE" "$MESSAGE"
}

# msg_error(title error_message)
#  * title: a title for the error message
#  * error_message: description of the error
#
# Example:
#   msg_error("FATAL ERROR" A not so long error message)
function msg_error() {
    TITLE=${1:-"ERROR"}
    MESSAGE=${@:2}
    msg_builder "bold,red" "white" "$TITLE" "$MESSAGE"
}

# msg_error_and_example(title error_message example)
#  * title: a title for the error message
#  * error_message: description of the error
#  * example: example code
#
# Example:
#   msg_error_and_example()
function msg_error_and_example() {
    TITLE=${1:-"ERROR"}
    MESSAGE=$2
    EXAMPLE=$3
    DIVIDER=$(color bold,red $(msg_horizontal_border ${#EXAMPLE}+2))

    echo -e $DIVIDER'
 '$(color bold,red $TITLE)':
  '$(color white $MESSAGE)'
    
 '$(color bold,white EXAMPLE)':
  '$(color blue $EXAMPLE)'\n'$DIVIDER'\n'
}

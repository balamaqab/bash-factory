# Bash factory

A repository of Bash scripts (libs) to build new Bash projects. 

## Requirements
* [Rust and Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)
* [`bash_bundler`](https://lib.rs/crates/bash_bundler) installed from Cargo

## Features
* **Bundler.sh**: script to bundle all the project code in a one file script.
* **New.sh:** script to create a new project file from a template.

## How to

### Install Rust and the bash_bundler script
On Linux and macOS systems, run:

```bash
./install.sh
```


### Create a bundle

You can create a bundle from any script at `projects` folder. To do it you just need a run the bundler.sh script with the project name as first argument:

```bash
./bundler.sh wp-migrator
```

All the bundle projects are created in `builds/` folder

#### To bundle the project's scripts
To bundle the factory's scripts (inside `src/` folder) you should add `-src` as first argument. All the factory's scripts are created in the root folder.

```bash
./bundler.sh -src new
```

### Create a new project
To start a new project using the default template, you should run the following command:

```bash
./new.sh project
```

* The only argument is the project type, that can be `project` or `library`.


**Examples:**
```bash
# Create a template for a project
./new.sh project
# Create a template for a library
./new.sh library
```

* To create a project inside `demos` folder, you should use `demos/` as prefix when the wizard ask for the Project ID (e.g. `demos/test`)

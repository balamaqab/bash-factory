#!/usr/bin/env bash

###########################################################
#
# BUNDLER - v22.07.2
# Run the bundler script for a specific project
#
# EXAMPLE
# ./bundler wp-migrator
#
###########################################################

##########################################################
#
# Colors - v2022.07.0
#
# FUNCTIONS
#   * color()
#
###########################################################

__colors_normal=$(tput sgr0)
__colors_bold=$(tput bold)
__colors_underline=$(tput smul)
__colors_nounderline=$(tput rmul)

__colors_color_reset="${__colors_normal}\e[0m"
__colors_color_gray="30"
__colors_color_red="31"
__colors_color_green="32"
__colors_color_yellow="33"
__colors_color_blue="34"
__colors_color_pink="35"
__colors_color_cyan="36"
__colors_color_white="37"
__colors_color_38="38"
__colors_color_39="39"
__colors_color_40="40"

# color(format message)
#  * format: comma separated values with format information.
#     * Text styles available: normal, dim, bold, italic and underline.
#     * Text color available: red, yellow, green, blue, white, pink, cyan and gray
#  * message: any text or variable
#
# Example:
#   echo -e "$(color bold,red FATAL ERROR!) Unexpected error at line $(color yellow 24)."
function color() {
    format=$1
    content=${@:2}
    IFS=',' read -ra styles <<<"$format"

    new_message=""
    color_intensity=0
    for style in "${styles[@]}"; do
        case $style in
        normal)
            color_intensity=0
            ;;
        bold)
            color_intensity=1
            ;;
        dim)
            color_intensity=2
            ;;
        italic)
            color_intensity=3
            ;;
        underline)
            color_intensity=4
            ;;
        underline)
            new_message="$new_message${__colors_underline}"
            ;;
        *)
            var_style="__colors_color_$style"
            new_message="$new_message\033[${color_intensity};${!var_style}m"
            ;;
        esac
    done
    new_message="$new_message$content${__colors_color_reset}"

    echo "$new_message"
}
###########################################################
#
# Messages - v2022.07.0
# (description here)
#
# FUNCTIONS
#  * error_and_example()
#
###########################################################

# IMPORTS
##########################################################
#
# Colors - v2022.07.0
#
# FUNCTIONS
#   * color()
#
###########################################################

__colors_normal=$(tput sgr0)
__colors_bold=$(tput bold)
__colors_underline=$(tput smul)
__colors_nounderline=$(tput rmul)

__colors_color_reset="${__colors_normal}\e[0m"
__colors_color_gray="30"
__colors_color_red="31"
__colors_color_green="32"
__colors_color_yellow="33"
__colors_color_blue="34"
__colors_color_pink="35"
__colors_color_cyan="36"
__colors_color_white="37"
__colors_color_38="38"
__colors_color_39="39"
__colors_color_40="40"

# color(format message)
#  * format: comma separated values with format information.
#     * Text styles available: normal, dim, bold, italic and underline.
#     * Text color available: red, yellow, green, blue, white, pink, cyan and gray
#  * message: any text or variable
#
# Example:
#   echo -e "$(color bold,red FATAL ERROR!) Unexpected error at line $(color yellow 24)."
function color() {
    format=$1
    content=${@:2}
    IFS=',' read -ra styles <<<"$format"

    new_message=""
    color_intensity=0
    for style in "${styles[@]}"; do
        case $style in
        normal)
            color_intensity=0
            ;;
        bold)
            color_intensity=1
            ;;
        dim)
            color_intensity=2
            ;;
        italic)
            color_intensity=3
            ;;
        underline)
            color_intensity=4
            ;;
        underline)
            new_message="$new_message${__colors_underline}"
            ;;
        *)
            var_style="__colors_color_$style"
            new_message="$new_message\033[${color_intensity};${!var_style}m"
            ;;
        esac
    done
    new_message="$new_message$content${__colors_color_reset}"

    echo "$new_message"
}

# FUNCTIONS

# msg_horizontal_border(length character)
#  * length: default 42
#  * character: default "-"
#
# Example:
#   msg_horizontal_border(50 "=")
function msg_horizontal_border() {
    DIVIDER_LENGTH=${1:-42}
    DIVIDER_CHAR=${2:-"-"}
    DIVIDER=""

    for ((i = 0; i < $DIVIDER_LENGTH; ++i)); do
        DIVIDER=$DIVIDER$DIVIDER_CHAR
    done

    echo $DIVIDER
}

# msg_border(color character content)
#  * color
#  * character
#
# Example:
#   msg_border(dim,yellow "=" Any text that you want to wrap)
function msg_border() {
    CONTENT=${@:3}
    COLOR=${1:-"white"}
    CHARACTER=${2:-"-"}
    DIVIDER_TEXT=$(msg_horizontal_border ${#CONTENT}+2 $CHARACTER)
    DIVIDER=$(color $COLOR $DIVIDER_TEXT)
    echo -e $DIVIDER'\n '$CONTENT'\n'$DIVIDER'\n'
}

# msg_builder(title_color message_color title message)
#  * title_color
#  * message_color
#  * title
#  * message
#
# Example:
#   msg_builder(bold,yellow yellow Warning! A warning)
function msg_builder() {
    TITLE_COLOR=${1:-"bold,white"}
    MESSAGE_COLOR=${2:-"normal,white"}
    TITLE=$3
    MESSAGE=${@:4}
    DIVIDER=$(color $TITLE_COLOR $(msg_horizontal_border ${#MESSAGE}+2))

    echo -e $DIVIDER'\n '$(color $TITLE_COLOR $TITLE)'\n  '$(color $MESSAGE_COLOR $MESSAGE)'\n'$DIVIDER'\n'
}

# msg_informative(title message)
#  * title: a title for the warning message
#  * message: description of the error
#
# Example:
#   msg_informative("Tip" Did you know that...?)
function msg_informative() {
    TITLE=${1:-"Info"}
    MESSAGE=${@:2}
    msg_builder "bold,blue" "white" "$TITLE" "$MESSAGE"
}

# msg_warning(title warning_message)
#  * title: a title for the warning message
#  * warning_message: description of the error
#
# Example:
#   msg_warning("WARNING!" A not so long warning message)
function msg_warning() {
    TITLE=${1:-"Warning!"}
    MESSAGE=${@:2}
    msg_builder "bold,yellow" "yellow" "$TITLE" "$MESSAGE"
}

# msg_error(title error_message)
#  * title: a title for the error message
#  * error_message: description of the error
#
# Example:
#   msg_error("FATAL ERROR" A not so long error message)
function msg_error() {
    TITLE=${1:-"ERROR"}
    MESSAGE=${@:2}
    msg_builder "bold,red" "white" "$TITLE" "$MESSAGE"
}

# msg_error_and_example(title error_message example)
#  * title: a title for the error message
#  * error_message: description of the error
#  * example: example code
#
# Example:
#   msg_error_and_example()
function msg_error_and_example() {
    TITLE=${1:-"ERROR"}
    MESSAGE=$2
    EXAMPLE=$3
    DIVIDER=$(color bold,red $(msg_horizontal_border ${#EXAMPLE}+2))

    echo -e $DIVIDER'
 '$(color bold,red $TITLE)':
  '$(color white $MESSAGE)'
    
 '$(color bold,white EXAMPLE)':
  '$(color blue $EXAMPLE)'\n'$DIVIDER'\n'
}

# Defaults
MODE=projects # projects | core
SET_PERMS=1
RUN_AFTER_BUILD=0
# Standard projects
PROJECTS_PATH=./projects
BUILDS_PATH=./builds
# Core projects
SOURCE_PATH=./src
SOURCE_BUILD_PATH=.

LAST_POSITION=1
while getopts 's:t:crp:' option "$@"; do
    case $option in
    's')
        PROJECTS_PATH=$OPTARG
        ;;
    't')
        BUILDS_PATH=$OPTARG
        ;;
    'r')
        RUN_AFTER_BUILD=1
        ;;
    'p')
        SET_PERMS=${OPTARG:-1}
        ;;
    'c')
        msg_informative "MODE CORE" "Bundling from $SOURCE_PATH"
        MODE="core"
        PROJECTS_PATH=$SOURCE_PATH
        BUILDS_PATH=$SOURCE_BUILD_PATH
        ;;
    esac
    if [ $OPTIND -gt $LAST_POSITION ]; then
        LAST_POSITION=$OPTIND
    fi
done

SCRIPTS=${@:$LAST_POSITION}
for MAIN_SCRIPT in ${SCRIPTS[@]}; do
    MAIN_SCRIPT_PATH=""
    MAIN_SCRIPT_NAME="${MAIN_SCRIPT##*/}"
    MAIN_SCRIPT_PATH="${MAIN_SCRIPT%%$MAIN_SCRIPT_NAME}"

    SOURCE_FILE="$PROJECTS_PATH/$MAIN_SCRIPT_PATH$MAIN_SCRIPT_NAME.sh"
    BUILD_FILE="$BUILDS_PATH/$MAIN_SCRIPT_NAME.sh"

    msg_informative "$(color bold,white 'Building') $(color bold,red $MAIN_SCRIPT'...')" "$(color white $SOURCE_FILE) $(color bold,yellow '-->') $(color bold,red $BUILD_FILE)"

    bash_bundler $SOURCE_FILE >$BUILD_FILE

    if [ $SET_PERMS -eq 1 ]; then
        chmod +x $BUILD_FILE
    fi

    if [ $RUN_AFTER_BUILD -eq 1 ]; then
        $BUILD_FILE
    fi
done

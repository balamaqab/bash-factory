#!/usr/bin/env bash
###########################################################
#
# SSH Tunnels Demo - v2022.07.0
# Demonstration for SSH tunnels library
#
# EXAMPLE
# (Example here)
#
###########################################################

# IMPORTS
# import ../../libs/network/ssh-tunnels.sh

PROCESS_ID=$(open_ssh_tunnel $(whoami)@localhost 80 8081)
echo "SSH Tunnel opened as process $PROCESS_ID"

sleep 10s

echo "Closing SSH Tunnel"
close_ssh_tunnel

#!/usr/bin/env bash

##########################################################
#
# Demo of Colors - v2022.07.0
# Demostration of function from colors library
#
# EXAMPLE
# ./builds/colors.sh
#
############################################################

# IMPORTS
# import ../../libs/appeareance/colors.sh

echo -e "$(color bold,yellow --------------------------------)"
echo -e "\t$(color bold,green A) $(color bold,yellow colorful) $(color bold,red demo)"
echo -e "$(color bold,yellow --------------------------------)"
echo " "

text_styles=("normal" "bold" "dim" "italic" "underline")
text_colors=("red" "yellow" "green" "blue" "white" "pink" "cyan" "gray")

divider="\t | \t"

for text_color in ${text_colors[@]}; do
    line=""
    for text_style in ${text_styles[@]}; do
        line="$line$(color $text_style,$text_color $text_style $text_color)$divider"
    done
    echo -e $line
done
echo -e "\n"

#!/usr/bin/env bash
###########################################################
#
# Messages demo - v2022.07.0
# A demonstration project for the Messages library
#
###########################################################

# IMPORTS
# import ../../libs/appeareance/messages.sh

INTERVAL=1s

msg_informative "Did you know...?" "This is an informative message"
sleep $INTERVAL
msg_warning "Warning" "A small warning ;)"
sleep $INTERVAL
msg_warning "A long warning" "You should use a third argument for that! Ok?"
sleep $INTERVAL
msg_error "FATAL ERROR" A not so long error message
sleep $INTERVAL
msg_error_and_example "ERROR" "The first argument are required." "$0 first_argument"
sleep $INTERVAL
msg_border "bold,pink" '=' "Anything you want!"

#!/usr/bin/env bash

# Rust installation
curl https://sh.rustup.rs -sSf | sh

# bash_bundler installation
cargo install bash_bundler

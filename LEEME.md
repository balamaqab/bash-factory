# Bash factory

Un repositorio de scripts de Bash (libs/) para crear nuevos proyectos de Bash.

## Requerimientos
* [Rust y Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html) instalados.
* [`bash_bundler`](https://lib.rs/crates/bash_bundler) instalado desde Cargo.

## Características
* **Bundler.sh**: script para empaquetar todo el código de un proyecto en un solo archivo.
* **New.sh:** script para crear un nuevo proyecto desde una plantilla.

## Instrucciones

### Instalar dependencias
En Linux y MacOs puedes ejecutar el siguiente comando:
```bash
./install.sh
```

### Para importar scripts 
Para importar scripts desde tu proyecto, debes usar la siguiente sintaxis:
```bash
# import ../libs/example.sh
```

### Crear el paquete de un proyecto

Puedes crear un paquete de cualquier script en la carpeta `projects`. Para hacerlo, solo debes ejecutar el script bundler.sh  con el nombre del proyecto como primer argumento:

```bash
./bundler.sh nombre-del-proyecto
```

All the bundle projects are created in `builds/` folder

#### Cómo empaquetar los scripts de la Bash Factory
Para empaquetar los scripts de la Bash Factory (adentro del folder `src/`) tú debes agregar el argumento `-src` al inicio. Todos los scripts empaquetados se guardarán en la carpeta principal del proyecto, en lugar de la carpeta `builds`.

```bash
./bundler.sh -src new
```

### Crear un nuevo proyecto
Para comenzar un nuevo proyecto usando la plantilla por defecto, tú debes ejecutar el siguiente comando:

```bash
./new.sh project
```
* El primer y único argumento es el tipo de proyecto, que puede ser `project` o `library`.

**Ejemplos:**
```bash
# Crear un nuevo proyecto
./new.sh project
# Crear una nueva librería
./new.sh library
```

* Si deseas crear un proyecto dentro de la carpeta `demos`, cuando te solicite el **Project ID** debes colocar `demos/` antes del id, por ejemplo: `demos/prueba`


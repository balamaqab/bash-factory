#!/usr/bin/env bash

###########################################################
#
# BUNDLER - v22.07.2
# Run the bundler script for a specific project
#
# EXAMPLE
# ./bundler wp-migrator
#
###########################################################

# import ../libs/appeareance/colors.sh
# import ../libs/appeareance/messages.sh

# Defaults
MODE=projects # projects | core
SET_PERMS=1
RUN_AFTER_BUILD=0
# Standard projects
PROJECTS_PATH=./projects
BUILDS_PATH=./builds
# Core projects
SOURCE_PATH=./src
SOURCE_BUILD_PATH=.

LAST_POSITION=1
while getopts 's:t:crp:' option "$@"; do
    case $option in
    's')
        PROJECTS_PATH=$OPTARG
        ;;
    't')
        BUILDS_PATH=$OPTARG
        ;;
    'r')
        RUN_AFTER_BUILD=1
        ;;
    'p')
        SET_PERMS=${OPTARG:-1}
        ;;
    'c')
        msg_informative "MODE CORE" "Bundling from $SOURCE_PATH"
        MODE="core"
        PROJECTS_PATH=$SOURCE_PATH
        BUILDS_PATH=$SOURCE_BUILD_PATH
        ;;
    esac
    if [ $OPTIND -gt $LAST_POSITION ]; then
        LAST_POSITION=$OPTIND
    fi
done

SCRIPTS=${@:$LAST_POSITION}
for MAIN_SCRIPT in ${SCRIPTS[@]}; do
    MAIN_SCRIPT_PATH=""
    MAIN_SCRIPT_NAME="${MAIN_SCRIPT##*/}"
    MAIN_SCRIPT_PATH="${MAIN_SCRIPT%%$MAIN_SCRIPT_NAME}"

    SOURCE_FILE="$PROJECTS_PATH/$MAIN_SCRIPT_PATH$MAIN_SCRIPT_NAME.sh"
    BUILD_FILE="$BUILDS_PATH/$MAIN_SCRIPT_NAME.sh"

    MSG_TITLE="$(color bold,white 'Building') $(color bold,blue $MAIN_SCRIPT'...')"
    MSG_BODY="$(color white $SOURCE_FILE) $(color bold,red '-->') $(color bold,yellow $BUILD_FILE)"
    msg_informative $MSG_TITLE $MSG_BODY
    bash_bundler $SOURCE_FILE >$BUILD_FILE

    if [ $SET_PERMS -eq 1 ]; then
        chmod +x $BUILD_FILE
    fi

    if [ $RUN_AFTER_BUILD -eq 1 ]; then
        $BUILD_FILE
    fi
done

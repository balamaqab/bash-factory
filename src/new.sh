#!/usr/bin/env bash

###########################################################
#
# NEW - v22.07.2
# Make a new project with a default template
#
# EXAMPLE
# ./new.sh project
#
###########################################################

# import ../libs/appeareance/colors.sh
# import ../libs/appeareance/messages.sh

if [ "$1" == "" ]; then
    msg_error_and_example "ERROR" "The first argument are required: type." "$0 project"
    exit 400
fi

#
# VARIABLES
#
PROJECT_TYPE=${1:-"project"}
PROJECT_TYPE_NAME="Project"
PROJECTS_PATH="./projects"

if [ "$PROJECT_TYPE" == "library" ]; then
    PROJECT_TYPE_NAME="Library"

    echo -en "$(color bold,white Category) $(color dim,white [appeareance]): "
    read LIBRARY_CATEGORY
    LIBRARY_CATEGORY=${LIBRARY_CATEGORY:-"appeareance"}
    PROJECTS_PATH="./libs/$LIBRARY_CATEGORY/"
fi

echo -en "$(color bold,white $PROJECT_TYPE_NAME ID): "
read PROJECT_ID

MAIN_SCRIPT_NAME="${PROJECT_ID##*/}"
if [ "$PROJECT_TYPE" == "project" ]; then
    MAIN_SCRIPT_PATH="${PROJECT_ID%%$MAIN_SCRIPT_NAME}"
    PROJECTS_PATH=$PROJECTS_PATH/$MAIN_SCRIPT_PATH
fi

echo -en "$(color bold,white $PROJECT_TYPE_NAME Name): "
read PROJECT_NAME
PROJECT_NAME=${PROJECT_NAME:-"(PROJECT NAME)"}

echo -en "$(color bold,white Description): "
read DESCRIPTION
DESCRIPTION=${DESCRIPTION:-"(description here)"}

DEFAULT_VERSION=$(date +"%Y.%m.0")
echo -en "$(color bold,white Version) $(color dim,white [$DEFAULT_VERSION]): "
read VERSION
VERSION=${VERSION:-$DEFAULT_VERSION}

OUTPUT_FILE="$PROJECTS_PATH/$MAIN_SCRIPT_NAME.sh"

case $PROJECT_TYPE in
project)
    echo -e '#!/usr/bin/env bash
###########################################################
#
# '$PROJECT_NAME' - v'$VERSION'
# '$DESCRIPTION'
#
# EXAMPLE
# (Example here)
#
###########################################################

# IMPORTS
# import ../libs/appeareance/colors.sh # example

# START YOUR CODE HERE

' >$OUTPUT_FILE
    ;;
library)
    echo -e '
###########################################################
#
# '$PROJECT_NAME' - v'$VERSION'
# '$DESCRIPTION'
#
# FUNCTIONS
#  * '$PROJECT_ID'()
#
###########################################################

# IMPORTS
# import ../appeareance/colors.sh # Importing colors as example

# VARIABLES
# use a prefix like __'$PROJECT_ID'_my_variable

__'$PROJECT_ID'_default_name=$(whoami)

# FUNCTIONS

function '$PROJECT_ID'() {
    # Arguments:
    #  * name: Username
    #
    # Example:
    #   '$PROJECT_ID'("Wendy")
    
    name=${1:-$__'$PROJECT_ID'_default_name}
    echo "Hello @name!"
}

' >$OUTPUT_FILE
    ;;
esac
